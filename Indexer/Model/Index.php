<?php
declare(strict_types=1);

namespace Module\Indexer\Model;

use Module\Indexer\Api\Data\IndexInterface;

class Index extends \Magento\Framework\Model\AbstractModel implements IndexInterface
{

    public function _construct()
    {
        $this->_init(\Module\Indexer\Model\ResourceModel\Index::class);
    }

    /**
     * @return array|mixed|null
     */
    public function getId()
    {
        return $this->getData(self::INDEX_ID);
    }

    /**
     * @return array|mixed|null
     */
    public function getProductId()
    {
        return $this->getData(self::INDEX_PRODUCT_ID);
    }

    /**
     * @return array|mixed|null
     */
    public function getResult()
    {
        return $this->getData(self::INDEX_RESULT);
    }

    /**
     * @param $id
     * @return Index|mixed
     */
    public function setId($id)
    {
        return $this->setData(self::INDEX_ID, $id);
    }

    /**
     * @param $productId
     * @return Index|mixed
     */
    public function setProductId($productId)
    {
        return $this->setData(self::INDEX_PRODUCT_ID, $productId);
    }

    /**
     * @param $result
     * @return Index|mixed
     */
    public function setResult($result)
    {
        return $this->setData(self::INDEX_RESULT, $result);
    }
}
