<?php

namespace Module\Indexer\Model\Indexer;

use \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use \Magento\Framework\Indexer\ActionInterface as IndexerInterface;
use \Magento\Framework\Mview\ActionInterface as MviewInterface;

class CustomProductIndex implements IndexerInterface, MviewInterface
{
    const TEST_CUSTOM_PRODUCT_INDEX_TABLE_NAME = 'test_custom_product_index';

    /**
     * @var CollectionFactory
     */
    private $productCollectionFactory;
    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    private $resource;
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * CustomProductIndex constructor.
     * @param CollectionFactory $productCollectionFactory
     * @param \Magento\Framework\App\ResourceConnection $resource
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        CollectionFactory $productCollectionFactory,
        \Magento\Framework\App\ResourceConnection $resource,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->resource = $resource;
        $this->logger = $logger;
    }

    public function executeFull()
    {
        try {
            $connection = $this->resource->getConnection();
            $connection->truncateTable($connection->getTableName(self::TEST_CUSTOM_PRODUCT_INDEX_TABLE_NAME));

            $collection = $this->productCollectionFactory->create();
            $collection->addAttributeToSelect('*');

            $indexData = [];
            foreach ($collection as $product) {
                $indexData[] = [
                    'product_id' => $product->getId(),
                    'result' => $product->getId() % 2 ? 0 : 1
                ];
            }
            $connection->insertMultiple(
                $connection->getTableName(self::TEST_CUSTOM_PRODUCT_INDEX_TABLE_NAME),
                $indexData
            );
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }

    /**
     * @param array $ids
     */
    public function executeList(array $ids)
    {
        try {
            $connection = $this->resource->getConnection();

            $connection->delete(
                $connection->getTableName(self::TEST_CUSTOM_PRODUCT_INDEX_TABLE_NAME),
                ['id IN (?)' => $ids]
            );

            $collection = $this->productCollectionFactory->create();
            $collection->addAttributeToSelect('*');
            $collection->addFieldToFilter('entity_id', ['in' => $ids]);
            $indexData = [];
            foreach ($collection as $product) {
                $indexData[] = [
                    'product_id' => $product->getId(),
                    'result' => $product->getId() % 2 ? 0 : 1
                ];
            }
            $connection->insertMultiple(
                $connection->getTableName(self::TEST_CUSTOM_PRODUCT_INDEX_TABLE_NAME),
                $indexData
            );
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }

    /**
     * @param int $id
     */
    public function executeRow($id)
    {
        try {
            $connection = $this->resource->getConnection();
            $connection->delete(
                $connection->getTableName(self::TEST_CUSTOM_PRODUCT_INDEX_TABLE_NAME),
                ['id = ?' => $id]
            );

            $collection = $this->productCollectionFactory->create();
            $collection->addAttributeToSelect('*');
            $collection->addFieldToFilter('entity_id', ['eq' => $id]);
            $indexData = [];
            foreach ($collection as $product) {
                $indexData[] = [
                    'product_id' => $product->getId(),
                    'result' => $product->getId() % 2 ? 0 : 1
                ];
            }
            $connection->insertMultiple(
                $connection->getTableName(self::TEST_CUSTOM_PRODUCT_INDEX_TABLE_NAME),
                $indexData
            );
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }

    /**
     * @param int[] $ids
     */
    public function execute($ids)
    {
        $this->executeList($ids);
    }
}
