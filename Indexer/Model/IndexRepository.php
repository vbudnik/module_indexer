<?php

declare(strict_types=1);

namespace Module\Indexer\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Module\Indexer\Api\Data;
use Module\Indexer\Api\IndexRepositoryInterface;
use Module\Indexer\Model\IndexFactory;
use Module\Indexer\Model\ResourceModel\Index\CollectionFactory;
use Module\Indexer\Model\ResourceModel\Index as ResourceIndex;

class IndexRepository implements IndexRepositoryInterface
{

    /**
     * @var ResourceIndex
     */
    private $resource;

    /**
     * @var \Module\Indexer\Model\IndexFactory
     */
    private $indexFactory;

    /**
     * @var CollectionFactory
     */
    private $indexCollectionFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var Data\IndexSearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * IndexRepository constructor.
     * @param ResourceIndex $resource
     * @param \Module\Indexer\Model\IndexFactory $indexFactory
     * @param CollectionFactory $indexCollectionFactory
     * @param Data\IndexSearchResultsInterfaceFactory $searchResult
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceIndex $resource,
        IndexFactory $indexFactory,
        CollectionFactory $indexCollectionFactory,
        Data\IndexSearchResultsInterfaceFactory $searchResult,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->indexFactory = $indexFactory;
        $this->indexCollectionFactory = $indexCollectionFactory;
        $this->searchResultsFactory = $searchResult;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @param Data\IndexInterface $index
     * @return Data\IndexInterface|mixed
     * @throws CouldNotSaveException
     */
    public function save(Data\IndexInterface $index)
    {
        try {
            $this->resource->save($index);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }
        return $index;
    }

    /**
     * @param $id
     * @return Index|mixed
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        $index = $this->indexFactory->create();
        $this->resource->load($index, $id);
        if (!$index->getId()) {
            throw new NoSuchEntityException(__('The index with the "%1" ID doesn\'t exist.', $id));
        }
        return $index;
    }

    /**
     * @param SearchCriteriaInterface $criteria
     * @return Data\IndexSearchResultsInterface|mixed
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        $collection = $this->indexCollectionFactory->create();
        $this->collectionProcessor->process($criteria, $collection);
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @param Data\IndexInterface $index
     * @return bool|mixed
     * @throws CouldNotDeleteException
     */
    public function delete(Data\IndexInterface $index)
    {
        try {
            $this->resource->delete($index);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @param $id
     * @return bool|mixed
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }
}
