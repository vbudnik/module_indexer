<?php

namespace Module\Indexer\Model\ResourceModel\Index;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    public function _construct()
    {
        $this->_init(\Module\Indexer\Model\Index::class, \Module\Indexer\Model\ResourceModel\Index::class);
    }
}
