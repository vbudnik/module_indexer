<?php

namespace Module\Indexer\Model\ResourceModel;

class Index extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    public function _construct()
    {
        $this->_init('test_custom_product_index', 'id');
    }
}
