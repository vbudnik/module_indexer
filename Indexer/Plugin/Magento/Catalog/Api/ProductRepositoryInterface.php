<?php

namespace Module\Indexer\Plugin\Magento\Catalog\Api;

use Magento\Catalog\Api\ProductRepositoryInterface as Subject;
use Module\Indexer\Api\IndexRepositoryInterface;
use Magento\Catalog\Api\Data\ProductExtensionFactory;

class ProductRepositoryInterface
{
    /**
     * @var ProductExtensionFactory
     */
    private $productExtensionFactory;
    /**
     * @var IndexRepositoryInterface
     */
    private $indexRepository;

    /**
     * ProductRepositoryInterface constructor.
     * @param ProductExtensionFactory $productExtensionFactory
     * @param IndexRepositoryInterface $customProductIndexRepository
     */
    public function __construct(
        ProductExtensionFactory $productExtensionFactory,
        IndexRepositoryInterface $customProductIndexRepository
    ) {
        $this->productExtensionFactory = $productExtensionFactory;
        $this->indexRepository = $customProductIndexRepository;
    }

    /**
     * @param Subject $subject
     * @param $result
     * @return mixed
     */
    public function afterGet(Subject $subject, $result)
    {
        $product = $result;
        $customProductIndex = $this->indexRepository->getById($product->getId());
        if ($product->getExtensionAttributes() && $product->getExtensionAttributes()->getResult()) {
            return $product;
        }

        if (!$product->getExtensionAttributes()) {
            $productExtension = $this->productExtensionFactory->create();
            $prodcuct->setExtensionAttributes($productExtension);
        }

        $product->getExtensionAttributes()->setResult($customProductIndex->getResult());
        return $product;
    }
}
