<?php

namespace Module\Indexer\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface IndexSearchResultsInterface extends SearchResultsInterface
{

    /**
     * @return \Magento\Framework\Api\ExtensibleDataInterface[]
     */
    public function getItems();

    /**
     * @param array $items
     * @return IndexSearchResultsInterface
     */
    public function setItems(array $items);
}
