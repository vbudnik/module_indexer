<?php

namespace Module\Indexer\Api\Data;

interface IndexInterface
{

    const INDEX_ID = 'id';
    const INDEX_PRODUCT_ID = 'product_id';
    const INDEX_RESULT = 'result';

    /**
     * @return mixed
     */
    public function getId();

    /**
     * @return mixed
     */
    public function getProductId();

    /**
     * @return mixed
     */
    public function getResult();

    /**
     * @param $id
     * @return mixed
     */
    public function setId($id);

    /**
     * @param $productId
     * @return mixed
     */
    public function setProductId($productId);

    /**
     * @param $result
     * @return mixed
     */
    public function setResult($result);
}
