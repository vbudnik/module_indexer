<?php

namespace Module\Indexer\Api;

interface IndexRepositoryInterface
{

    /**
     * @param Data\IndexInterface $index
     * @return mixed
     */
    public function save(Data\IndexInterface $index);

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);
    
    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return mixed
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * @param Data\IndexInterface $index
     * @return mixed
     */
    public function delete(Data\IndexInterface $index);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteById($id);
}
